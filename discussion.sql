-- List of Databases

SHOW DATABASES;

-- Create a database
-- Syntax:
    -- CREATE DATABASE <name_db>
CREATE DATABASE music_db;

-- drop databas , deletion of database
-- syntax:
    -- drop database <name_db>

-- select a database
-- use <name_db>
USE music_db;

-- create tables
-- table columns will also be declared here
CREATE TABLE users(
    -- INT to indicate that the id column will have integer entry
    id INT NOT NULL AUTO_INCREMENT,
    username VARCHAR(50) NOT NULL,
    password VARCHAR(50) NOT NULL,
    full_name VARCHAR(50) NOT NULL,
    contact_number CHAR(11) NOT NULL,
    email VARCHAR(50) NOT NULL,
    address VARCHAR(50),
    -- To set the primary key
    PRIMARY KEY (id)

);

-- drop a table from database
DROP TABLE userss;

CREATE TABLE playlists (
    id INT NOT NULL AUTO_INCREMENT,
    user_id INT NOT NULL,
    playlist_name VARCHAR(30) NOT NULL,
    datetime_created DATETIME NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_playlist_user_id
        FOREIGN KEY (user_id) REFERENCES users(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
)

-- artists table
CREATE TABLE artists (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL,
    PRIMARY KEY (id)
)

-- Mini-activity for the albums
-- columns: id, album_title, date_released, artist_id
    -- CASCADE
    -- Cannot delete the artist
    -- declare yung foreing key
CREATE TABLE albums (
    id INT NOT NULL AUTO_INCREMENT,
    album_title VARCHAR(50) NOT NULL,
    date_released DATETIME NOT NULL,
    artist_id VARCHAR(50) NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_albums_artist_id
    FOREIGN KEY (artist_id) REFERENCES artists(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
)


CREATE TABLE songs (
    id INT NOT NULL AUTO_INCREMENT,
    song_name VARCHAR(50) NOT NULL,
    length TIME NOT NULL,
    genre INT NOT NULL,
    album_id INT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_songs_album_id
    FOREIGN KEY (album_id) REFERENCES albums(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
)

CREATE TABLE playlist_songs (
    id INT NOT NULL AUTO_INCREMENT,
    playlist_id INT NOT NULL,
    song_id INT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_playlists_songs_playlist_id
    FOREIGN KEY (playlist_id) REFERENCES playlists(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT,
    PRIMARY KEY (id),
    CONSTRAINT fk_playlists_songs_playlist_id
    FOREIGN KEY (playlist_id) REFERENCES playlists(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
)




